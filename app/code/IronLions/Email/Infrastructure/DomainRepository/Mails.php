<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Infrastructure\DomainRepository;

use IronLions\Email\Domain\Mail;
use IronLions\Email\Infrastructure\Persistence\MailQueue;
use Phalcon\Di;

final class Mails implements \IronLions\Email\Domain\Repository\Mails
{
    /** @var MailQueue */
    private $db;

    /**
     * Mails constructor.
     */
    public function __construct()
    {
        $this->db = Di::getDefault()->get('ironlions.email.infra.database.mailqueue');
    }

    /**
     * @param Mail $mail
     * @return int
     */
    public function add(Mail $mail): int
    {
        return $this->db->persist($mail);
    }
}
