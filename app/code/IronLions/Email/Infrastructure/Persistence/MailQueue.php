<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright  Copyright (c) Lamia Oy (https://lamia.fi)
 * @author     Niko Grano <niko@lamia.fi>
 */

namespace IronLions\Email\Infrastructure\Persistence;


use IronLions\Email\Domain\Mail;

interface MailQueue
{
    public function persist(Mail $mail);
}