<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright  Copyright (c) Lamia Oy (https://lamia.fi)
 * @author     Niko Grano <niko@lamia.fi>
 */

namespace IronLions\Email\Infrastructure\Database;

use IronLions\Email\Domain\Exception\Database\PersistingFailedException;
use IronLions\Email\Domain\Mail;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Sender;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Send;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Headers;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Attachments;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Message;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Recipient;
use IronLions\Email\Infrastructure\Persistence\MailQueue as MailQueueInterface;
use Phalcon\Mvc\User\Component;

final class MailQueue extends Component implements MailQueueInterface
{
    /**
     * @param Mail $mail
     * @throws PersistingFailedException
     */
    public function persist(Mail $mail): void
    {
        $this->db->begin();

        $sender = (new Sender())->setSender($mail->getSender());
        if (!$sender->save()) {
            $this->db->rollback();
            throw new PersistingFailedException(
                (string)$sender->getMessages()
            );
        }

        $headers = (new Headers())->setHeaders($mail->getHeaders());
        if (!$headers->save()) {
            $this->db->rollback();
            throw new PersistingFailedException(
                (string)$headers->getMessages()
            );
        }

        $attachments = (new Attachments())->setAttachments($mail->getAttachments());
        if (!$attachments->save()) {
            $this->db->rollback();
            throw new PersistingFailedException(
                (string)$attachments->getMessages()
            );
        }

        $send = (new Send())->setSend($mail->getSend());
        if (!$send->save()) {
            $this->db->rollback();
            throw new PersistingFailedException(
                (string)$send->getMessages()
            );
        }

        $message = (new Message())->setMessage($mail->getMessage());
        if (!$message->save()) {
            $this->db->rollback();
            throw new PersistingFailedException(
                (string)$message->getMessages()
            );
        }

        $recipient = (new Recipient())->setRecipient($mail->getRecipient());
        if (!$recipient->save()) {
            $this->db->rollback();
            throw new PersistingFailedException(
                (string)$recipient->getMessages()
            );
        }

        $this->db->commit();

        var_dump($sender->getId());
        die();
    }
}
