<?php

namespace IronLions\Email\Infrastructure\Database\Resources\Mail;
use IronLions\Email\Domain\Recipient as RecipientEntity;
use Phalcon\Mvc\Model;

final class Recipient extends Model
{
    public const TABLE = 'il_email_mailqueue';

    /** @var int */
    private $id;

    /** @var array */
    private $To;

    /** @var array */
    private $Cc;

    /** @var array */
    private $Bcc;

    public function initialize(): void
    {
        $this->setSource(self::TABLE);
    }

    public function setRecipient(RecipientEntity $recipient): Recipient
    {
        $this->To = implode(',', $recipient->getTo());
        $this->Cc = implode(',', $recipient->getCc());
        $this->Bcc = implode(',' ,$recipient->getBcc());

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }
}
