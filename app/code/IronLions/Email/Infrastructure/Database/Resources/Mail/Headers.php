<?php

namespace IronLions\Email\Infrastructure\Database\Resources\Mail;
use IronLions\Email\Domain\Headers as HeadersEntity;
use Phalcon\Mvc\Model;

final class Headers extends Model
{
    public const TABLE = 'il_email_mailqueue';

    /** @var int */
    private $id;

    /** @var string */
    private $language;

    /** @var string */
    private $priority;

    /** @var string */
    private $encoding;

    /** @var object */
    private $custom;

    public function initialize(): void
    {
        $this->setSource(self::TABLE);
    }

    public function setHeaders(HeadersEntity $headers): Headers
    {
        $this->language = $headers->getLanguage();
        $this->priority = $headers->getPriority();
        $this->encoding = $headers->getEncoding();
        $this->custom = $headers->getCustom();

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }
}
