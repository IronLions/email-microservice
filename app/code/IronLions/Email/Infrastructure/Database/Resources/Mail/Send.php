<?php

namespace IronLions\Email\Infrastructure\Database\Resources\Mail;
use IronLions\Email\Domain\Send as SendEntity;
use Phalcon\Mvc\Model;

final class Send extends Model
{
    public const TABLE = 'il_email_mailqueue';

    /** @var int */
    private $id;

    /** @var mixed */
    private $send;

    public function initialize(): void
    {
        $this->setSource(self::TABLE);
    }

    public function setSend(SendEntity $send): Send
    {
        $this->send = $send->getSend();

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }
}
