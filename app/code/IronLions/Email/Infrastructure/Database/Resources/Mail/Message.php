<?php

namespace IronLions\Email\Infrastructure\Database\Resources\Mail;
use IronLions\Email\Domain\Message as MessageEntity;
use Phalcon\Mvc\Model;

final class Message extends Model
{
    public const TABLE = 'il_email_mailqueue';

    /** @var int */
    private $id;

    /** @var string */
    private $subject;

    /** @var string */
    private $content;

    public function initialize(): void
    {
        $this->setSource(self::TABLE);
    }

    public function setMessage(MessageEntity $message): Message
    {
        $this->subject = $message->getSubject();
        $this->content = $message->getContent();

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }
}
