<?php

namespace IronLions\Email\Infrastructure\Database\Resources\Mail;
use IronLions\Email\Domain\Sender as SenderEntity;
use Phalcon\Mvc\Model;

final class Sender extends Model
{
    public const TABLE = 'il_email_mailqueue';

    /** @var int */
    private $id;

    /** @var string */
    private $from;

    /** @var string */
    private $replyTo;

    public function initialize(): void
    {
        $this->setSource(self::TABLE);
    }

    public function setSender(SenderEntity $sender): Sender
    {
        $this->from = $sender->getFrom();
        $this->replyTo = $sender->getReplyTo();

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }
}