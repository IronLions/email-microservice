<?php

namespace IronLions\Email\Infrastructure\Database\Resources\Mail;
use IronLions\Email\Domain\Attachments as AttachmentsEntity;
use Phalcon\Mvc\Model;

final class Attachments extends Model
{
    public const TABLE = 'il_email_mailqueue';

    /** @var int */
    private $id;

    /** @var string */
    private $content;

    /** @var string */
    private $filename;

    public function initialize(): void
    {
        $this->setSource(self::TABLE);
    }

    public function setAttachments(AttachmentsEntity $Attachments): Attachments
    {
        $this->content = $Attachments->getContent();
        $this->filename = $Attachments->getFilename();

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }
}
