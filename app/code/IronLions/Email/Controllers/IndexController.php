<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Controllers;

use Interna\Rest\Mvc\AbstractRestController;

final class IndexController extends AbstractRestController
{
    public function indexAction(): void
    {
        $this->returnError(501);
    }

    public function notFoundAction(): void
    {
        $this->returnError(404);
    }
}
