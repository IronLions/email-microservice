<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Controllers;

use Interna\Rest\Mvc\AbstractRestController;
use IronLions\Email\Application\Command\CreateMail;

final class MailSendController extends AbstractRestController
{
    public function MailSendAction(): void
    {
        $body = \json_decode(\file_get_contents('php://input'));

        if (null === $body) {
            $this->returnError(400);

            return;
        }
        try {
            $sender = new \IronLions\Email\Domain\Sender($body->sender->from, $body->sender->replyTo);
            $message = new \IronLions\Email\Domain\Message($body->message->subject, $body->message->content);
            $recipient = new \IronLions\Email\Domain\Recipient(
                $body->recipient->to ?? [],
                $body->recipient->cc ?? [],
                $body->recipient->bcc ?? []
            );

            $headers = isset($body->headers) ? new \IronLions\Email\Domain\Headers(
                $body->headers->language ?? 'en_GB',
                $body->headers->priority ?? '3',
                $body->headers->encoding ?? 'UTF8',
                $body->headers->custom ?? []
            ) : null;

            $attachments = (isset($body->attachments) && null !== $body->attachments->content && null !== $body->attachments->filename) ?
                new \IronLions\Email\Domain\Attachments(
                    $body->attachments->content,
                    $body->attachments->filename
            ) : null;

            $send = new \IronLions\Email\Domain\Send('c');

            $this->di->get('bus')->handle(
                new CreateMail(
                    $sender,
                    $recipient,
                    $headers,
                    $message,
                    $attachments,
                    $send,
                    function ($createdId) use (&$id): void {
                        $id = $createdId;
                    }
                )
            );

            if (null === $id) {
                $this->returnError(500);

                return;
            }

            $this->returnError(202);

            return;
        } catch (\IronLions\Email\Domain\Exception\InvalidArgumentException $invalidArgumentException) {
            $this->returnError(400, $invalidArgumentException->getMessage());

            return;
        }
    }

    public function notFoundAction(): void
    {
        \http_response_code(404);
        $this->view->pick('index/notFound');
    }
}
