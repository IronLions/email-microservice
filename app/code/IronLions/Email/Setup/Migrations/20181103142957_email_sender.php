<?php


use \Interna\Core\Setup\AbstractMigration;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Sender;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Send;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Message;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Headers;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Recipient;
use IronLions\Email\Infrastructure\Database\Resources\Mail\Attachments;

final class EmailSender extends AbstractMigration
{
    public function change(): void
    {
        $this->table(Sender::TABLE)
            ->addColumn('from', 'string', ['limit' => '254'])
            ->addColumn('replyTo', 'string', ['limit' => '254'])
            ->create();

        $this->table(Message::TABLE)
            ->addColumn('subject', 'string', ['limit' => '254'])
            ->addColumn('content', 'string', ['limit' => '254'])
            ->create();

        $this->table(Headers::TABLE)
            ->addColumn('language', 'string', ['limit' => '254'])
            ->addColumn('priority', 'string', ['limit' => '254'])
            ->addColumn('encoding', 'string', ['limit' => '254'])
            ->addColumn('custom', 'string', ['limit' => '254'])
            ->create();

        $this->table(Send::TABLE)
            ->addColumn('send', 'string', ['limit' => '254'])
            ->create();

        $this->table(Recipient::TABLE)
            ->addColumn('To', 'string', ['limit' => '4096'])
            ->addColumn('Cc', 'string', ['limit' => '4096'])
            ->addColumn('Bcc', 'string', ['limit' => '4096'])
            ->create();

        $this->table(Attachments::TABLE)
            ->addColumn('content', 'string', ['limit' => '4096'])
            ->addColumn('filename', 'string', ['limit' => '4096'])
            ->create();
    }
}
