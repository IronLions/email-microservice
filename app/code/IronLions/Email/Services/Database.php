<?php
/**
 * Created by PhpStorm.
 * User: niko
 * Date: 11/3/18
 * Time: 3:57 PM
 */

namespace IronLions\Email\Services;
use Interna\Core\Services\AbstractService;
use IronLions\Email\Infrastructure\Database\MailQueue;

final class Database extends AbstractService
{
    public function registerServices(\Phalcon\DiInterface $dependencyInjector): void
    {
        $dependencyInjector->set(
            'ironlions.email.infra.database.mailqueue',
            new MailQueue(),
            true
        );
    }
}