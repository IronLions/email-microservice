<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Application\Command;

use Interna\Core\CommandBus\CommandBusInterface;
use IronLions\Email\Domain\Mail;
use IronLions\Email\Infrastructure\DomainRepository\Mails;

final class CreateMailHandler implements CommandBusInterface
{
    private $mails;

    public function __construct(Mails $mails)
    {
        $this->mails = $mails;
    }

    /**
     * @param $command
     * @throws \IronLions\Email\Domain\Exception\ErrorAddingToDatabaseException
     * @return void
     */
    public function handle($command): void
    {
        /** @var CreateMail $command */
        $id = $this->mails->add(new Mail(
            $command->getSender(),
            $command->getRecipient(),
            $command->getHeaders(),
            $command->getMessage(),
            $command->getAttachments(),
            $command->getSend()
        ));

        $command->getCallback()($id);
    }
}
