<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

use IronLions\Email\Domain\Exception\InvalidArgumentException;

final class Sender
{
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $replyTo;

    public function __construct(string $from, string $replyTo)
    {
        if ('' === $from) {
            throw new InvalidArgumentException('Sender<from> was invalid.');
        }
        $this->from = $from;
        $this->replyTo = $replyTo;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getReplyTo(): string
    {
        return $this->replyTo;
    }
}
