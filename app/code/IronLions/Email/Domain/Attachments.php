<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

use InvalidArgumentException;

final class Attachments
{
    /**
     * @var string
     */
    private $content;
    /**
     * @var string
     */
    private $filename;

    public function __construct(string $content, string $filename)
    {
        if ('' === $content || \mb_strlen($content) > 254) {
            throw new InvalidArgumentException('Content<content> was invalid.');
        }
        if ('' === $filename) {
            throw new InvalidArgumentException('Filename<filename> was invalid.');
        }
        $this->content = $content;
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }
}
