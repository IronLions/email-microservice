<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

use IronLions\Email\Domain\Exception\InvalidArgumentException;

final class Headers
{
    /**
     * @var string
     */
    private $language;
    /**
     * @var string
     */
    private $priority;
    /**
     * @var string
     */
    private $encoding;
    /**
     * @var string[]
     */
    private $custom;

    public function __construct(string $language, string $priority, string $encoding, array $custom)
    {
        if (!(\array_keys($custom) !== \range(0, \count($custom) - 1)))
        {
            throw new InvalidArgumentException('Headers must be header:value form.');
        }

        $this->language = $language;
        $this->priority = $priority;
        $this->encoding = $encoding;
        $this->custom = $custom;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getPriority(): string
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getEncoding(): string
    {
        return $this->encoding;
    }

    /**
     * @return string[]
     */
    public function getCustom(): array
    {
        return $this->custom;
    }
}
