<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain\Recipient;

use Ds\Set;
use IronLions\Email\Domain\Human\Email;

final class Bcc
{
    /** @var Set */
    private $data;

    public function __construct()
    {
        $this->data = new Set();
    }

    public function add(Email $bcc): void
    {
        $this->data->add((string)$bcc);
    }

    public function get(): Set
    {
        return $this->data;
    }
}
