<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

final class Mail
{
    /**
     * @var Sender
     */
    private $sender;
    /**
     * @var Recipient
     */
    private $recipient;
    /**
     * @var Headers
     */
    private $headers;
    /**
     * @var Message
     */
    private $message;
    /**
     * @var Attachments|null
     */
    private $attachments;
    /**
     * @var Send
     */
    private $send;

    public function __construct(
        Sender $sender,
        Recipient $recipient,
        Headers $headers,
        Message $message,
        ?Attachments $attachments,
        Send $send
    )
    {
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->headers = $headers;
        $this->message = $message;
        $this->attachments = $attachments;
        $this->send = $send;
    }

    /**
     * @return Sender
     */
    public function getSender(): Sender
    {
        return $this->sender;
    }

    /**
     * @return Recipient
     */
    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    /**
     * @return Headers
     */
    public function getHeaders(): Headers
    {
        return $this->headers;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @return Attachments|null
     */
    public function getAttachments(): ?Attachments
    {
        return $this->attachments;
    }

    /**
     * @return Send
     */
    public function getSend(): Send
    {
        return $this->send;
    }
}
