<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

use InvalidArgumentException;

final class Message
{
    /**
     * @var string
     */
    private $subject;
    /**
     * @var string
     */
    private $content;

    public function __construct(string $subject, string $content)
    {
        if ('' === $subject || \mb_strlen($subject) > 254) {
            throw new InvalidArgumentException('Subject<subject> was invalid.');
        }
        if ('' === $content) {
            throw new InvalidArgumentException('Content<content> was invalid.');
        }
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
