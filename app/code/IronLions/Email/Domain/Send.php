<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

final class Send
{
    /**
     * @var date-time
     */
    private $send;

    public function __construct(string $format)
    {
        $d = new \DateTimeImmutable();
        $this->send = $d->format($format);
    }

    /**
     * @return mixed
     */
    public function getSend()
    {
        return $this->send;
    }
}
