<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */

namespace IronLions\Email\Domain;

use IronLions\Email\Domain\Human\Email;
use IronLions\Email\Domain\Recipient\Bcc;
use IronLions\Email\Domain\Recipient\Cc;
use IronLions\Email\Domain\Recipient\To;

final class Recipient
{
    /**
     * @var To
     */
    private $to;
    /**
     * @var Cc
     */
    private $cc;
    /**
     * @var Bcc
     */
    private $bcc;

    public function __construct(array $to, array $cc, array $bcc)
    {
        $this->to = new To();
        $this->cc = new Cc();
        $this->bcc = new Bcc();

        foreach ($to as $item) {
            $this->to->add(new Email($item));
        }

        foreach ($cc as $item) {
            $this->to->add(new Email($item));
        }

        foreach ($bcc as $item) {
            $this->to->add(new Email($item));
        }
    }

    /**
     * @return To
     */
    public function getTo(): To
    {
        return $this->to;
    }

    /**
     * @return Cc
     */
    public function getCc(): Cc
    {
        return $this->cc;
    }

    /**
     * @return Bcc
     */
    public function getBcc(): Bcc
    {
        return $this->bcc;
    }
}
