<?php

declare(strict_types=1);

/**
 * Interna Core — PHP Framework on Phalcon — NOTICE OF LICENSE
 * This source file is released under EUPL 1.2 license by copyright holders.
 * Please see LICENSE file for more specific information about terms.
 *
 * @copyright 2017-2018 (c) Niko Granö (https://granö.fi)
 * @copyright 2017-2018 (c) IronLions (https://ironlions.fi)
 */
final class index
{
    public function __construct()
    {
        \chdir(\dirname(__DIR__));
        require \dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
        $env = 'production' !== \mb_strtolower(false === \getenv('APP_ENV') ? 'production' : \getenv('APP_ENV'));
        $flag = \file_exists(\dirname(__DIR__).DIRECTORY_SEPARATOR.'development.env');
        $dev = $flag || $env;
        new Interna(
            'yes' === \getenv('CONFIG_CACHE') || !$dev,
            $dev,
            $dev ? 7 : 3
        );
    }
}
new index();
